.. ReveCMS documentation master file, created by
   sphinx-quickstart on Wed Mar 28 11:39:05 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ReveCMS documentation
=====================
This documentation covers the installation and configuration of a ReveCMS test environment. It has been verified to work on Ubuntu Linux 11.10. ReveCMS is a Django app and therefore requires python, Django and several other software packages.

Prepare your environment
------------------------
Install dependencies

``sudo apt-get install mercurial git python-reportlab python-dev sqlite3``

Install pip

``sudo easy_install pip``

Install virtualenv

``sudo pip install virtualenv virtualenvwrapper``

Check your python version (should be 2.6 or 2.7)

``python --version``

Logout and login (or run ``. ~/.bashrc`` ) to create virtualenvwrapper scripts

Create a virtual environment and make it active

``mkvirtualenv revecms``

``workon revecms``

Download ReveCMS
----------------
Create or edit the .hgrc file in your homedirectory to set mercurial defaults

``nano ~/.hgrc``::

 [ui]
 username = yourname <your.email@example.com>
 
 [auth]
 bb1.prefix = https://bitbucket.org/
 bb1.username = yourname
 bb1.password = yourpassword

Clone the ReveCMS repository

``hg clone https://bitbucket.org/lhilarides/django-revecms-cocap-virtualenv``

Install ReveCMS
---------------

Django bootstrap

``cd ~/django-revecms-cocap-virtualenv``

``python bootstrap.py``

Create folders for media and static files

``cd ~/django-revecms-cocap-virtualenv``

``mkdir .media .static``

Optional: edit settings

``nano  ~/django-revecms-cocap-virtualenv/settings.py``

Initialise database (default is sqlite, editable in settings.py)

``python manage.py syncdb`` When requested to create superuser, follow instructions

``python manage.py make_homepage``

``python manage.py runserver --insecure 0.0.0.0:8000``

Work with ReveCMS
------------------

Start a browser and enter the address: http://localhost:8000 or http://127.0.0.1:8000

.. toctree::
   :maxdepth: 2

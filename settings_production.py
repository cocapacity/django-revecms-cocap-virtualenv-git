from settings import *

# turn off debugging (! Important !)
DEBUG = False
TEMPLATE_DEBUG = DEBUG

# database setting
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': "DBNAME_PROD",
        'USER': 'USERNAME',
        'PASSWORD': 'PASSWORD',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

# cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': 'unix:~/memcached.sock',
        'OPTIONS': {
            'KEY_PREFIX': "prod"
        }
    }
}

# email
EMAIL_HOST = "HOSTNAME"
EMAIL_HOST_USER = "NOREPLY"
EMAIL_HOST_PASSWORD = "PASSWORD"
DEFAULT_FROM_EMAIL = "NOREPLY@YOURDOMAIN.COM"
SERVER_EMAIL = "NOREPLY@YOURDOMAIN.COM"

# media directory
MEDIA_ROOT = os.path.join(PROJECT_ROOT, "..", "..", "django_production_media")

# static directory
STATIC_ROOT = os.path.join(PROJECT_ROOT, "..", "..", "django_production_static")

# installed apps without debug_toolbar??

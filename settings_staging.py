from settings import *

# turn on debugging
DEBUG = True
TEMPLATE_DEBUG = DEBUG

# database setting
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': "DBNAME_STAGING",
        'USER': 'USERNAME',
        'PASSWORD': 'PASSWORD',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

# cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': 'unix:~/memcached.sock',
        'OPTIONS': {
            'KEY_PREFIX': "staging"
        }
    }
}

# media directory
MEDIA_ROOT = os.path.join(PROJECT_ROOT, "..", "..", "django_staging_media")

# static directory
STATIC_ROOT = os.path.join(PROJECT_ROOT, "..", "..", "django_staging_static")

import os
import xmlrpclib

from fabric.api import *
from fabric.contrib.project import rsync_project
from fabric.contrib import files, console
from fabric import utils
from fabric.operations import prompt
from fabric.decorators import hosts


RSYNC_EXCLUDE = (
    '.hg',
    '*.pyc',
    '*.example',
    '*.db',
    'fabfile.py',
    'bootstrap.py',
    '*.wsgi',
    'whoosh_index/',
)

def _setup_xmlrpc():
    # connect to rpc server
    server = xmlrpclib.ServerProxy('https://api.webfaction.com/')
    
    # prompt for password
    prompt("Enter password for %s on %s!" % (env.user, env.hosts), "password")
    
    # log in
    session_id, account = server.login(env.user, env.password)    
    
    return (server, session_id, account)

def _setup_path():
    """
    setup some variables, directories and paths
    """
    # application name
    env.application = 'django_%(environment)s' % env
    # settings file
    env.settings = 'sign.settings_%(environment)s' % env 
    
    # home folder
    env.home = "/home/sign"
    # root application folder
    env.app_root = os.path.join(env.home, 'webapps', env.application)
    # code path
    env.code_root = os.path.join(env.app_root, "sign")
    # virtual env
    env.virtualenv_root = os.path.join(env.app_root, '.virtualenvs')
    
    # path to apache bin
    env.apache = os.path.join(env.app_root, "apache2", "bin")   
    

def staging():
    """ use staging environment on remote host """
    # environment
    env.environment = 'staging'
    
    # host and user
    env.user = 'sign'
    env.hosts = ['sign.webfactional.com']
    
    # domains we run under, PRIMARY FIRST!
    env.domains = {'innovatieglastuinbouw.nl': ['staging']}
    
    # setup path
    _setup_path()


def production():
    """ use production environment on remote host """
    # environment
    env.environment = 'production' 
    
    # host and user
    env.user = 'sign'
    env.hosts = ['sign.webfactional.com']
    
    # domains we run under, PRIMARY FIRST!
    env.domains = {'innovatieglastuinbouw.nl': ['www']}
    
    
    # setup path
    _setup_path()
    
def setup_webfaction():
    """ initilize webfaction environment """
    require('user', provided_by=('staging', 'production'))
    require('hosts', provided_by=('staging', 'production'))
    require('domains', provided_by=('staging', 'production'))
    
    server, session_id, account = _setup_xmlrpc()
    
    # create domain if needed
    utils.puts("create domains")
    cdl = server.list_domains(session_id)
    for domain, subs in env.domains.items():
        for sub in [d["subdomains"] for d in cdl if d["domain"] == domain]:
            subs.extend(sub)
        server.create_domain(session_id, domain, *tuple(set(subs)))
        
    # create applications
    utils.puts("create applications")
    known_apps = [a["name"] for a in server.list_apps(session_id)]
    if 'django_%(environment)s' % env not in known_apps:
        server.create_app(session_id, 'django_%(environment)s' % env, 'django131_mw33_27', False, '')
    
    if 'django_%(environment)s_media' % env not in known_apps:
        server.create_app(session_id, 'django_%(environment)s_media' % env, 'static_only', False, '')
   
    if 'django_%(environment)s_static' % env not in known_apps:
        server.create_app(session_id, 'django_%(environment)s_static' % env, 'static_only', False, '')

        
    # create database
    utils.puts("create databases")
    known_dbs = [(db["name"], db["db_type"]) for db in server.list_dbs(session_id)]
    if ('%(user)s_django_%(environment)s' % env, "postgresql") not in known_dbs: 
        prompt("Enter database password?" , "dbpassword")
        server.create_db (session_id, '%(user)s_django_%(environment)s' % env, 'postgresql', env.dbpassword)
        
    # create website
    utils.puts("create websites")
    known_websites = []
    if 'django_%(environment)s' % env in known_websites:
        # ip
        ip = [ entry["ip"] for entry in server.list_ips(session_id) if entry["is_main"]][0]
        
        # domain list
        domains = list()
        for domain, subs in env.domains.items():
            for sub in subs:
                domains.append("%s.%s" % (sub, domain))
                
        server.create_website(session_id, 'django_%(environment)s' % env, ip, False, domains, 
                              [
                                  ['django_%(environment)s' % env,'/'],
                                  ['django_%(environment)s_media' % env,'/media'],
                                  ['django_%(environment)s_static' % env,'/static']
                               ]
                              )
        
    # mailboxes
    utils.puts("create mailboxes")
    mail_box = '%(user)s_noreply' % env
    known_mailboxes = [a["mailbox"] for a in server.list_mailboxes(session_id)]
    if mail_box not in known_mailboxes:
        ret = server.create_mailbox(session_id, mail_box)
        utils.put("create mailbox: %(name)s with password: %(password)s")
    
    # emailaddresses
    utils.puts("create emailaddress")
    email = 'noreply@%s' % env.domains.keys()[0]
    known_emails = [e["email_address"] for e in server.list_emails(session_id)]
    if email not in known_emails:
        server.create_email(session_id, email, mail_box)


def bootstrap():
    """ initialize remote host environment (virtualenv, deploy, update, wsgi) """
    require('app_root', provided_by=('staging', 'production'))
    run('mkdir -p %(app_root)s' % env)
    create_virtualenv()
    deploy()
    update_requirements()
    create_wsgi()


def create_virtualenv():
    """ setup virtualenv on remote host """
    require('virtualenv_root', provided_by=('staging', 'production'))
    args = '--clear --distribute'
    run('virtualenv %s %s' % (args, env.virtualenv_root))


def deploy():
    """ rsync code to remote host """
    require('app_root', provided_by=('staging', 'production'))
    if env.environment == 'production':
        if not console.confirm('Are you sure you want to deploy production?',
                               default=False):
            utils.abort('Production deployment aborted.')
    # defaults rsync options:
    # -pthrvz
    # -p preserve permissions
    # -t preserve times
    # -h output numbers in a human-readable format
    # -r recurse into directories
    # -v increase verbosity
    # -z compress file data during the transfer
    extra_opts = '--omit-dir-times'
    rsync_project(
        env.app_root,
        exclude=RSYNC_EXCLUDE,
        delete=True,
        extra_opts=extra_opts,
    )
    
    
def create_wsgi():
    require('code_root', provided_by=('staging', 'production'))
    require('virtualenv_root', provided_by=('staging', 'production'))
    require('settings', provided_by=('staging', 'production'))
    # create project.wsgi
    files.upload_template(
        'mod_wsgi.tmpl',
        '%s/project.wsgi' % env.code_root,
        context=env
    )


def update_requirements():
    """ update external dependencies on remote host """
    require('code_root', provided_by=('staging', 'production'))
    requirements = os.path.join(env.code_root, 'requirements')
    with cd(requirements):
        cmd = ['pip install']
        cmd += ['-E %(virtualenv_root)s' % env]
        cmd += ['--requirement %s' % os.path.join(requirements, 'apps.txt')]
        run(' '.join(cmd))

def apache_start():    
    """ start Apache on remote host"""
    require('apache', provided_by=('staging', 'production'))
    cmd = os.path.join(env.apache, "start")
    run(cmd)

def apache_stop():    
    """ stop Apache on remote host """
    require('apache', provided_by=('staging', 'production'))
    cmd = os.path.join(env.apache, "stop")
    run(cmd)

def apache_restart():    
    """ restart Apache on remote host """
    require('apache', provided_by=('staging', 'production'))
    cmd = os.path.join(env.apache, "restart")
    run(cmd)

def collect_static():    
    """ colllect static """
    require('virtualenv_root', provided_by=('staging', 'production'))
    require('code_root', provided_by=('staging', 'production'))
    require('settings', provided_by=('staging', 'production'))
    pbin = os.path.join(env.virtualenv_root, 'bin', 'python')

    with cd(env.code_root):
        run('%s manage.py collectstatic --noinput --settings=%s' % 
            (pbin, env.settings))

def syncdb():    
    """ syncdb """
    require('virtualenv_root', provided_by=('staging', 'production'))
    require('code_root', provided_by=('staging', 'production'))
    require('settings', provided_by=('staging', 'production'))
    
    pbin = os.path.join(env.virtualenv_root, 'bin', 'python')
    with cd(env.code_root):
        run('%s manage.py syncdb --settings=%s' % 
            (pbin, env.settings))


def do_sshkey():
    """Install your SSH public key

    This is a wrapper around :command:`ssh-copy-id` which runs on all currently
    defined Fabric hosts.
    """
    import urlparse
    for host in env.all_hosts:
        o = urlparse.urlparse('svn+ssh://%s' % host)
        local("ssh-copy-id '%s@%s'" % (env.user, o.hostname))

# Django settings for amp project.
import os.path

PROJECT_ROOT = os.path.dirname(__file__)

INTERNAL_IPS = ('127.0.0.1',)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Joost Lieshout', 'joost.lieshout@co-capacity.org'),
    ('Lammert Hilarides', 'lammert.hilarides@co-capacity.org'),
    ('Michiel Bijland', 'michiel.bijland@co-capacity.org'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': os.path.join(PROJECT_ROOT, "test.db"),                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Amsterdam'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'nl-nl'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, ".media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_ROOT, ".static")

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, "static"),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 't8#hz0hg@f8rzsen_i%e5@za_(2@yds_@k1)_#1!3sx93mucs7'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, "templates"),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.comments',
    'django.contrib.humanize',
    'django.contrib.webdesign',
    'debug_toolbar',
    'mailer',
    'registration',
    # reve
    'rosetta',
    'pagination',
    'tinymce',
    'filebrowser',
    'haystack',
    'tagging',
    'contentmanager',
    'basicblocks',
    'extrablocks',
    'sitemanager',
    'newscast',
    'blogs',
    'events',
    'feedmaker',
    'filebrowser_plus',
    'iso3166',
    'iso639',
    'taggingviews',
    'syndicator',
    'maps',
    #'djangolungus',
    'tagcanvas',
    'htmlsanitizer',
#    'dynamic_scraper',
#    'open_news',
)


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

ACCOUNT_ACTIVATION_DAYS = 7
EMAIL_BACKEND = "mailer.backend.DbBackend"

# reve
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'contentmanager.middleware.EditmodeMiddleware',
    'pagination.middleware.PaginationMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

FORCE_LOWERCASE_TAGS = True

HAYSTACK_SITECONF = 'search_sites'
HAYSTACK_SEARCH_ENGINE = 'whoosh'
HAYSTACK_WHOOSH_PATH = os.path.join(PROJECT_ROOT, 'whoosh_index', str(SITE_ID), LANGUAGE_CODE)

CACHE_BACKEND = 'locmem://'

SITEMANAGER_TEMPLATES = [
    ('home.html','homepage'),
    ('3-col.html','three columns, wider middle column'),
    ('2-col-a.html','two columns, smaller left'),
    ('2-col-b.html','two columns, smaller right'),
    ('1-col.html','one column')
]
# CONTENT_MANAGER_KLASSES = (("%s ui-corner-all blockborder" % x,x) for x in ['blue', 'greenblue','green','yellow','orange','red'])

# django-filebrowser / uploadify use this
FILEBROWSER_MAX_UPLOAD_SIZE = 15728640 # 15 MB

# Note: the compressor doesn't seem to work with the tinymce jquery
# package.
TINYMCE_COMPRESSOR = False
# For the spellchecker to work you'll need py-enchant (+ enchant and
# dictionaries for the languases you want to support)
TINYMCE_SPELLCHECKER = False
TINYMCE_DEFAULT_CONFIG = {
    "mode": "exact",
    "theme" : "advanced",
    "plugins": "paste,searchreplace,table,safari,fullscreen", #,directionality,spellchecker",
    "paste_auto_cleanup_on_paste" : "true",
    "language" : "en",
    "button_tile_map" : True,
    "entity_encoding" : "raw",
    "remove_linebreaks" : True,
    "theme_advanced_buttons1_add" :"|,hr,blockquote,charmap,|,removeformat,cleanup,|,search,spellchecker",
    "theme_advanced_disable" :"styleselect",
    "theme_advanced_buttons2_add" : "|,directionality,fullscreen",
    "theme_advanced_buttons2_add_before": "cut,copy,paste,|,pastetext,pasteword,selectall,|",
    "theme_advanced_buttons3": "tablecontrols",
    "theme_advanced_toolbar_location" : "top",
    "theme_advanced_toolbar_align" : "left",
    "theme_advanced_styles": "",
    "theme_advanced_statusbar_location": "bottom",
    "theme_advanced_resizing" : True,
    "save_enablewhendirty": True,
    "auto_cleanup_word" : True,
    "auto_reset_designmode" : True,
    "extended_valid_elements" : "li[class|id],hr[class|size|noshade|color],"
    +"br[class|id|clear<all?left?none?right],"
    +"area[shape|href|onmouseover|onmouseout|coords],map[name],"
    +"img[id|class|src|border|alt|title|hspace|vspace|"
    +"width|height|align|onmouseover|onmouseout|name|usemap|style]",
    "invalid_elements" : "font",
    "remove_script_host" : True,
    "relative_urls" : False,
    }
FILEBROWSER_EXTENSIONS = {
    'Folder': [''],
    'Image': ['.jpg','.jpeg','.gif','.png','.tif','.tiff','.bmp'],
    'Video': ['.mov','.wmv','.mpeg','.mpg','.avi','.rm'],
    'Document': ['.pdf','.doc','.rtf','.txt','.xls','.csv','.ppt','.xlsx','.pptx','.docx','.odf','.odt','.ods','.odp','.odf'],
    'Audio': ['.mp3','.mp4','.wav','.aiff','.midi','.m4p'],
    'Code': ['.html','.py','.js','.css']
}

DEBUG_TOOLBAR_CONFIG = {"INTERCEPT_REDIRECTS":False}

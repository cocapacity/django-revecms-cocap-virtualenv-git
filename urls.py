from django.conf import settings
from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from django.views.decorators.cache import cache_page

from tinymce.views import compressor
from haystack.views import SearchView, search_view_factory
from haystack.forms import SearchForm


urlpatterns = patterns(
    '',
    # account and profile editing
    # (r'^accounts/', include('profiles.backends.profile.urls')),
    # reve
    (r'^tinymce/compressor/$', compressor),
    (r'^admin/filebrowser/meta/', include('filebrowser_plus.urls')),
    (r'^admin/filebrowser/', include('filebrowser.urls')),
    (r'^tinymce/', include('tinymce.urls')),
    (r'^rosetta/', include('rosetta.urls')),
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin/', include(admin.site.urls) ),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', name='logout'),
    (r'^comments/', include('django.contrib.comments.urls')),
    url(r'^search/',
        search_view_factory(
            view_class=SearchView,
            load_all=False,
            form_class=SearchForm
        ),
        name='haystack_search'),
    (r'^apps/events/', include('events.urls')),
    (r'^apps/news/', include('newscast.urls')),
    (r'^apps/feedmaker/', include('feedmaker.urls')),
    (r'^apps/tagging/', include('taggingviews.urls')),
    (r'^apps/syndicator/', include('syndicator.urls')),
    (r'^apps/blogs/', include('blogs.urls')),
    (r'^apps/maps/', include('maps.urls')),
    (r'^contentmanager/', include('contentmanager.urls')),
    (r'^sitemanager/', include('sitemanager.urls.manage')),
    (r'', include('sitemanager.urls.view')),
    )

# this is for serving media files in development
if settings.DEBUG:
    import os
    # get the static path from settings
#    static_url = settings.STATIC_URL
#    if static_url.startswith('/'):
#        static_url = static_url.lstrip('/')
#        urlpatterns += patterns(
#            '',
#            (r'^%s(?P<path>.*)$' % static_url, 'django.views.static.serve',
#             {'document_root': settings.STATIC_ROOT}),
#            )
    static_url = settings.MEDIA_URL
    if static_url.startswith('/'):
        static_url = static_url.lstrip('/')
        urlpatterns += patterns(
            '',
            (r'^%s(?P<path>.*)$' % static_url, 'django.views.static.serve',
             {'document_root': settings.MEDIA_ROOT}),
            )

from sitemanager.signals import url_changed
from contentmanager import autodiscover, blockpath_rename

autodiscover()

def url_change_listener(sender, **kwargs):
    oldpath = kwargs['oldpath']
    newpath = kwargs['newpath']
    blockpath_rename(oldpath, newpath)

url_changed.connect(url_change_listener)
